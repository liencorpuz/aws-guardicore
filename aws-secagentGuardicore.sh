#!/bin/bash

    #this is to add line at the end of a file
    #sudo sed -i -e '$a54.254.136.41    gc-aws.globe.com.ph' /etc/hosts
    this is to add line at the first line of the file PRIVATE IP of the NEW Aggregator Guardicore
    sed -i '1 i\172.31.19.206 gc-aws.globe.com.ph' /etc/hosts 
    #this is to add line at the first line of the file using PUBLIC IP of Guardicore
    #sed -i '1 i\18.142.89.155 gc-aws.globe.com.ph' /etc/hosts
#######################################################################################################################
echo -e "\nInstalling Guardicore Agent...............\n\n\n"
#######################################################################################################################
    export UI_UM_PASSWORD='P7fOLq2g'
    export GC_PROFILE='project_white'
    export SSL_ADDRESSES="gc-aws.globe.com.ph:443"
    curl -s -k -o /tmp/guardicore_cas_chain_file.pem https://gc-aws.globe.com.ph/guardicore-cas-chain-file.pem;
    # expected checksum fa44e976eca98f19d658d8d1ceb4939cd0906c6848c556238d06108c3c7b73b5
    SHA256SUM_VALUE=`sha256sum /tmp/guardicore_cas_chain_file.pem | awk '{print $1;}'`
    export INSTALLATION_CMD='curl -s --cacert /tmp/guardicore_cas_chain_file.pem https://gc-aws.globe.com.ph| bash'
    if [ $SHA256SUM_VALUE == fa44e976eca98f19d658d8d1ceb4939cd0906c6848c556238d06108c3c7b73b5 ]; then eval $INSTALLATION_CMD; else echo "Certificate checksum mismatch error"; fi
    sudo gc-agent status
