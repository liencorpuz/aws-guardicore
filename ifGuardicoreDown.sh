#!/bin/bash

    sudo cp -p /etc/default/guardicore /etc/default/guardicore.backup 
    sudo sed -i 's/export RUN_AS_PRIVILEGED="false"/export RUN_AS_PRIVILEGED="true"/g' /etc/default/guardicore
    sudo service gc-agent restart
    sudo gc-agent restart-all
    sudo gc-agent status